# 关于PSI

---

>PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。
>

# PSI演示

---

>PSI的演示见：<a target="_blank" href="http://psi.sturgeon.mopaasapp.com">http://psi.sturgeon.mopaasapp.com</a>
>
>PC端请用`360浏览器`或者是`谷歌浏览器`访问
> 
>移动端扫码访问![移动端扫码访问](PSI_Mobile_URL.png)

# PSI的开源协议

---

>PSI的开源协议为GPL v3
>

# PSI相关项目

---

> 1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help
>
> 2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 如需要技术支持，请联系

---

>1. 月付费VIP QQ群：482424398
>
> 加入本群的要求：100元/人/月;
>
> 服务内容:PSI相关开发技术细节咨询和指导;
>
> 自愿选择付费几个月，期限到后退群结束服务。
>
>2. 二次开发付费QQ群：414474186
>
>3. 实名VIP付费QQ群：108111233
>
> 加入本群的要求：实名
>
>4. email： crm8000@qq.com
>

# 致谢

---

>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、pinyin_php (https://gitee.com/cik/pinyin_php)
>
>6、PHPExcel (https://github.com/PHPOffice/PHPExcel)
>
>7、TCPDF (http://www.tcpdf.org/)
>
>8、Material-UI (https://material-ui.com/)
>
>9、UmiJS (https://umijs.org/)
>
>10、DVA (https://github.com/dvajs/dva)
